jekyll (4.3.4+dfsg-1) unstable; urgency=medium

  * debian/tests/control: remove fakeroot from test command
  * New upstream version 4.3.4+dfsg

 -- Antonio Terceiro <terceiro@debian.org>  Tue, 15 Oct 2024 20:03:53 -0300

jekyll (4.3.3+dfsg-1) unstable; urgency=medium

  * New upstream version 4.3.3+dfsg
    + fix for Ruby3.2+ struct initialisation
  * Remove duplicate word in d/rules
  * Remove XS-Ruby-Versions from d/control

 -- Cédric Boutillier <boutil@debian.org>  Fri, 26 Jul 2024 11:34:41 +0200

jekyll (4.3.2+dfsg-2) unstable; urgency=medium

  * Team upload
  * 0016-Drop-usage-of-safe_yaml.patch: add Symbol to the list of permitted
    classes in safe_load_yaml (Closes: #1030465)
  * clean test/source/.jekyll-cache to build successfully twice in a row
    (Closes: #1048945)

 -- Cédric Boutillier <boutil@debian.org>  Tue, 23 Jul 2024 16:27:22 +0200

jekyll (4.3.2+dfsg-1) unstable; urgency=medium

  [ Daniel Leidert ]
  * New upstream version 4.3.2+dfsg

  [ Sébastien Villemot ]
  * Allow YAML aliases (Closes: #1050867)

  [ Antonio Terceiro ]
  * Revert test changes meant for jekyll-sass-converter 3.x

 -- Antonio Terceiro <terceiro@debian.org>  Wed, 30 Aug 2023 15:12:35 -0300

jekyll (4.3.1+dfsg-2) unstable; urgency=medium

  * Team upload
  * debian/ruby-tests.rake: always run tests under TZ=UTC (Closes: #1034450)

 -- Antonio Terceiro <terceiro@debian.org>  Sun, 16 Apr 2023 18:35:56 -0300

jekyll (4.3.1+dfsg-1) unstable; urgency=medium

  * Team upload
  * New upstream version 4.3.1+dfsg
    - works with version of ruby-mercenary in the archive (Closes: #1017524 )
  * Refresh patches and add some more
    - Rebased on new upstream release, dropping patches that are not
      applicable anymore
    - Add some new patches with fixes and skipping some few tests that don't
      work on Debian.
    - Add patch to add support for ruby-liquid 5 (Closes: #1019634, #1026480)
  * debian/ruby-tests.rake: skip test for Windows
  * Bump build dependency on ruby-jekyll-sass-converter to >= 2.1
  * Add build dependency on ruby-terminal-table
  * autopkgtest: drop coffescript tests
  * Add patch to not install packages from rubygems by default on `jekyll new`
  * autopkgtest: run without needing root
  * Move jekyll-theme-minima to Recommends:
  * autopkgtest: add smoke-test to create and build an empty site
  * debian/rules: install using rubygems layout
  * Adapt patch to rubygems installation layout
  * Depend on ruby-liquid version fixing a regression affecting jekyll
  * Add upstream patch to drop usage of safe_yaml (Closes: #1026427)

 -- Antonio Terceiro <terceiro@debian.org>  Sat, 21 Jan 2023 23:31:58 -0300

jekyll (3.9.0+dfsg-5) unstable; urgency=medium

  [ Debian Janitor ]
  * Remove constraints unnecessary since buster:
    + Build-Depends: Drop versioned constraint on ruby-liquid.
    + jekyll: Drop versioned constraint on ruby-classifier-reborn, ruby-coderay,
      ruby-mime-types, ruby-pygments.rb, ruby-rdiscount, ruby-redcarpet,
      ruby-tomlrb and ruby-yajl in Depends.
    + jekyll: Drop versioned constraint on ruby-jekyll-coffeescript in Suggests.

  [ Pirate Praveen ]
  * webpack command failure should not be ignored
  * Fix build failure with webpack 5.0
  * Bump Standards-Version to 4.6.1 (no changes needed)

 -- Pirate Praveen <praveen@debian.org>  Tue, 24 May 2022 17:25:15 +0530

jekyll (3.9.0+dfsg-4) unstable; urgency=medium

  * Team upload.

  [ Debian Janitor ]
  * Update standards version to 4.5.1, no changes needed.
  * Remove 1 unused lintian overrides.

  [ Daniel Leidert ]
  * d/lintian-brush.conf: Set compat-release to oldstable to prevent dh
    bumps.
  * d/watch: Update and fix watch file.

  [ Lucas Kanashiro ]
  * Add ruby-webrick as a dependency
  * Add patch to support webrick 1.7
  * Add patch to fix test failure with Ruby 3

 -- Lucas Kanashiro <kanashiro@debian.org>  Tue, 01 Feb 2022 16:35:53 -0300

jekyll (3.9.0+dfsg-3) unstable; urgency=medium

  * d/jekyll.lintian-overrides: Ignore spare-manual-page hint.
  * d/jekyll.manpages: Install all new manual pages.
  * d/jekyll.1: Rename to d/man/jekyll.1 and split out d/man/jekyll-build.1,
    d/man/jekyll-clean.1, d/man/jekyll-doctor.1, d/man/jekyll-help.1,
    d/man/jekyll-new-theme.1, d/man/jekyll-new.1 and d/man/jekyll-serve.1.
  * d/man/jekyll-b.1, d/man/jekyll-hyde.1, d/man/jekyll-s.1,

 -- Daniel Leidert <dleidert@debian.org>  Fri, 30 Oct 2020 14:52:04 +0100

jekyll (3.9.0+dfsg-2) unstable; urgency=medium

  * Team upload.
  * d/clean: Add file and remove override target in d/rules.
  * d/rules (override_dh_auto_clean): Remove target.
  * d/patches/*.patch: Refresh and add Forwarded field.
  * d/patches/0027-Fix-test-for-pygments-2.7.1.patch: Add patch.
    - Build with pygments 2.7.1.
  * d/patches/0028-Fix-test-for-rouge-3.22.patch: Add patch.
    - Relax test to work with ruby-rouge 3.22 too.
  * d/patches/series: Enable new patches.
  * d/source/lintian-overrides: Override
    debian-watch-does-not-check-gpg-signature and
    package-uses-old-debhelper-compat-version.
  * d/tests/control: Name tests.

 -- Daniel Leidert <dleidert@debian.org>  Mon, 26 Oct 2020 19:32:39 +0100

jekyll (3.9.0+dfsg-1) unstable; urgency=medium

  * Team Upload

  [ Debian Janitor ]
  * Wrap long lines in changelog entries: 1.5.1-1.
  * Set field Upstream-Contact in debian/copyright.
  * Remove obsolete fields Contact, Name from debian/upstream/metadata
    (already present in machine-readable debian/copyright).

  [ Daniel Leidert ]
  * d/control (Build-Depends): Add ruby-coderay and ruby-yajl. Remove unused
    ruby-toml.
    (Depends): Replace most dependencies by ${ruby:Depends} except for those
    not listed in the gemspec but rather the Gemfile.
    (Suggests): Add jekyll-theme-minima, ruby-jekyll-avatar, and
    ruby-jekyll-mentions.
    (Description): Add a paragraph how to find the plugins.
  * d/ruby-tests.rake: Use gem2deb. Enable tag, convertible, plugin, and site
    tests. Enable coffeescript tests based on AUTOPKGTEST_TEST_COFFEESCRIPT
    environment variable. Enable the new command test based on the
    AUTOPKGTEST_TEST_NEW_COMMAND environment variable.
  * d/patches/0004_skip_test_autopkgtest.patch: Adjust patch.
    - Skip tests in test_site.rb which require the test-theme.
  * d/patches/0018-fix-test-configuration-autopkgtest.patch: Adjust patch.
    - Fix the path to site-template when running the test with autopkgtest.
  * d/patches/0021-Fix-ruby27-warnings.patch: Add patch.
    - Fiy Ruby 2.7 warnings.
  * d/patches/0022-escape-regex-characters-in-match.patch: New patch.
    - Convertible tests try to match some paths to output. Because our version
      contains a plus sign it must be escaped properly.
  * d/patches/0023-drop-custom-profile-simplecov.patch: Add new patch.
    - This custom profile somehow interfers with the tests and leads to errors.
      Worse the errors are not shown. So drop it.
  * d/patches/0024-use-platforms-in-gemfile.patch: Add patch.
    - Don't use :install_if in default Gemfile and use :platforms instead (see
      https://lists.debian.org/debian-ruby/2020/04/msg00062.html).
  * d/patches/0025-skip-plugin-tests-requiring-bundler.patch: Add patch.
    - Skip plugin manager test relying on bundler and upstream's Gemfile.
  * d/patches/series: Adjust accordingly.
  * d/tests/control: Change to run gem2deb-test-runner as command. By setting
    an enviromnent variable we set the tests to load in d/ruby-tests.rake. Add
    another test to run test_new_command.rb.
  * d/tests/coffeescript*: Remove files. This is handled by d/ruby-tests.rake.

  [ Cédric Boutillier ]
  * [ci skip] Update team name

  [ Pirate Praveen ]
  * New upstream version 3.9.0+dfsg (Closes: #961194, #966020)

 -- Pirate Praveen <praveen@debian.org>  Sat, 05 Sep 2020 14:19:21 +0530

jekyll (3.8.6+dfsg-3) unstable; urgency=medium

  * d/control (Uploaders): Add myself.
    (Standards-Version): Bump to 4.5.0.
    (Depends): Remove ruby-interpreter.

 -- Daniel Leidert <dleidert@debian.org>  Mon, 24 Feb 2020 21:29:16 +0100

jekyll (3.8.6+dfsg-2) unstable; urgency=medium

  * Team upload to unstable.
  * The tests run fine with ruby2.7 (closes: #952079).

 -- Daniel Leidert <dleidert@debian.org>  Mon, 24 Feb 2020 02:46:44 +0100

jekyll (3.8.6+dfsg-1) experimental; urgency=medium

  * Team upload.
  * New upstream release.
  * d/control: Add Rules-Requires-Root=no.
    (Build-Depends): Remove all packages depending on jekyll to prevent test
    issues. Fix uses-debhelper-compat-file.
    (Homepage): Update URL.
  * d/ruby-tests.rake: Exclude coffeescript tests. See README.source.
  * d/README.source: Explain the necessary test related changes.
  * d/patches/0015-Fix-CVE-2018-17567-Closes-909933.patch: Remove patch.
    - Fix has been applied by upstream.
  * debian/patches/0020-Skip-test-theme-tests.patch: Add patch.
    - Don't run test trying to load the test-theme theme.
  * d/patches/series: Remove patch.
  * d/source/lintian-overrides: Add some overrides.
  * d/tests/coffeescript, d/tests/coffeescript.rake: Run test_coffeescript.rb.
  * d/tests/control: Enable test.
  * d/upstream/metadata: Add basic metadata.

 -- Daniel Leidert <dleidert@debian.org>  Wed, 22 Jan 2020 23:06:07 +0100

jekyll (3.8.3+dfsg-8) unstable; urgency=medium

  * Team upload.
  * d/control (Recommends): Drop ruby-sequel* and ruby-mysql which are not
    used by jekyll anymore (leftovers of jekyll 0.x series).
    (Suggests): Drop ruby-jekyll-plugin for now.
  * d/jekyll.1: Massive update (closes: #948220).
  * d/p/0019-Compare-lists-ignoring-the-order.patch: Add patch.
    - Ignore list order when comparing Dir.glob() and Utils.safe_glob() to fix
      reprotest run.
  * d/p/series: Add patch.

 -- Daniel Leidert <dleidert@debian.org>  Fri, 10 Jan 2020 22:10:55 +0100

jekyll (3.8.3+dfsg-7) unstable; urgency=medium

  * Team upload.

  [ Utkarsh Gupta ]
  * Add salsa-ci.yml

  [ Daniel Leidert ]
  * d/compat: Raise to level 12.
  * d/control (Standards-Version): Bump to 4.4.1.
    (Build-Depends): Bump debhelper to version 12.
    (Depends): Move ruby-jekyll-coffeescript, ruby-jekyll-feed,
    ruby-jekyll-gist, and ruby-jekyll-paginate to Suggests (closes: #945334).
    Add more plugins to the suggested package list.
  * d/copyright: Major update.
    (Format): Use secure URI.
  * d/jekyll.lintian-overrides: Add overrides.
  * d/README.Debian: Rename to debian/README.source.
  * d/missing-sources/: Move the files into d/missing-sources/docs/js/.
  * d/p/0017-allow-jekyll-to-run-with-ruby-i18n-1.x.patch: Add patch.
    - Allow ruby-i18n 0.x and 1.x (closes: #948215).
  * d/p/0018-fix-test-configuration-autopkgtest.patch: Add patch.
    - Fix autopkgtest failure due to src/lib being removed.
  * d/p/series: Add patches.

 -- Daniel Leidert <dleidert@debian.org>  Tue, 07 Jan 2020 23:22:28 +0100

jekyll (3.8.3+dfsg-6) unstable; urgency=medium

  * Team upload
  * Bump Standards-Version to 4.4.0 (no changes needed)

 -- Pirate Praveen <praveen@debian.org>  Mon, 05 Aug 2019 05:55:43 +0000

jekyll (3.8.3+dfsg-5) experimental; urgency=medium

  * Build with webpack 4

 -- Pirate Praveen <praveen@debian.org>  Thu, 01 Aug 2019 21:11:29 +0200

jekyll (3.8.3+dfsg-4) unstable; urgency=medium

  * d/control: Add bundler to Depends (Closes: #924230)

 -- Youhei SASAKI <uwabami@gfd-dennou.org>  Sat, 16 Mar 2019 19:33:25 +0900

jekyll (3.8.3+dfsg-3.1) unstable; urgency=medium

  [ Jelmer Vernooĳ ]
  * Non-maintainer upload.
  * Use secure URI in Homepage field.

  [ Dr. Tobias Quathamer ]
  * Fix CVE-2018-17567, patch by Youhei SASAKI <uwabami@gfd-dennou.org>
    (Closes: #909933)

 -- Dr. Tobias Quathamer <toddy@debian.org>  Sat, 16 Feb 2019 13:46:27 +0100

jekyll (3.8.3+dfsg-3) unstable; urgency=medium

  [ Gianfranco Costamagna ]
  * Team upload (with maintainer permission)

  [ Simon Quigley ]
  * Fix the build failure by skipping the test that requires network access.
    Closes: #907322

 -- Simon Quigley <tsimonq2@ubuntu.com>  Mon, 10 Sep 2018 19:08:09 -0500

jekyll (3.8.3+dfsg-2) unstable; urgency=medium

  * Team upload
  * Reupload to unstable
  * Check gemspec dependencies on build

 -- Pirate Praveen <praveen@debian.org>  Thu, 23 Aug 2018 16:51:07 +0530

jekyll (3.8.3+dfsg-1) experimental; urgency=medium

  * Team upload

  [ Youhei SASAKI ]
  * d/patches: Refresh, Update

  [ Manas Kashyap ]
  * Import Upstream version 3.8.3
  * Standard version updated to 4.2.0
  * Added ruby-em-websocket as dependency in debian control file

  [ Pirate Praveen ]
  * Add ruby-httpclient and ruby-tomlrb to build-depends
  * Tighten dependency on ruby-liquid
  * Disable failing tests
  * Embed source for livereload.js
  * Build livereload.js from source using webpack
  * Exclude livereload.js from source tarball

 -- Pirate Praveen <praveen@debian.org>  Thu, 23 Aug 2018 16:32:36 +0530

jekyll (3.2.1+dfsg-1) unstable; urgency=medium

  * Imported Upstream version 3.2.1+dfsg
  * Refresh patches
  * Skip installation of files missing sources (removed upstream)
  * Depend on ruby-pathutil (TODO: to be packaged)
  * Exclude tests with test-theme (needs gemspec magic)
  * Attempt to fix jekyll with rouge2 (incomplete, WIP with upstream
    contributor). Still problems with codeblocks...

 -- Cédric Boutillier <boutil@debian.org>  Wed, 24 Aug 2016 17:22:11 +0200

jekyll (3.1.6+dfsg-3) unstable; urgency=medium

  * Team upload
  * Relax dependency for ruby-colorator

 -- Cédric Boutillier <boutil@debian.org>  Wed, 06 Jul 2016 22:42:24 +0200

jekyll (3.1.6+dfsg-2) unstable; urgency=medium

  * Team upload
  * Add adapt_test_rouge2.patch to fix a test for ruby-rouge 2.x

 -- Cédric Boutillier <boutil@debian.org>  Mon, 04 Jul 2016 12:04:06 +0200

jekyll (3.1.6+dfsg-1) unstable; urgency=medium

  * Team upload
  * Imported Upstream version 3.1.6+dfsg
  * Remove version in the gem2deb build-dependency
  * Set Section: to web
  * Use https:// in Vcs-* fields
  * Bump Standards-Version to 3.9.8 (no changes needed)
  * Run wrap-and-sort on packaging files
  * Drop paragraph about removed script/rebund in copyright file
  * Add repacksuffix option to debian/watch
  * Depend on ruby-lauchy-shim and drop 0003-Remove-Launchy-Dependencies.patch
  * Build-depend on ruby-nokogiri
  * Drop 0004-Fix-Gemfile-versioning-toml.patch (was fixing a change in a
    previous patch)
  * refresh patches
  * Add 0010-require-openssl.patch to require openssl in tests needing it
  * Add 0011-fix-test-pygments.patch to fix change of output in pygments
    (Closes: #825628)
  * Add 0012_no_require_relative.patch to use LOAD_PATH to find jekyll in
    tests
  * Add 0013_require_coffee_script_test.patch to require coffee_script for
    autopkgtests
  * Add 0014_skip_test_autopkgtest.patch to skip a failing test in autopkgtest
    environment

 -- Cédric Boutillier <boutil@debian.org>  Sun, 05 Jun 2016 11:33:15 +0200

jekyll (3.0.1+dfsg-1) unstable; urgency=medium

  [ Dominique Dumont ]
  * control: removed dod from uploaders

  [ Youhei SASAKI ]
  * Update debian/watch: use Github as upstream
  * Imported Upstream version 3.0.1
  * Refresh patches
    + Add patch: 0009-Remove-relative-LOAD_PATH-from-bin-jekyll.patch
  * Drop obsolete test-sources
  * Exclude test/test_convertible.rb
  * Install missing-sources to site_templates

 -- Youhei SASAKI <uwabami@gfd-dennou.org>  Sun, 22 Nov 2015 15:31:07 +0900

jekyll (2.2.0+dfsg-2) unstable; urgency=medium

  * Team upload.
  * Update Build-Depends ruby-activesupport-3.2
    to ruby-activesupport, which is the 4.0 version that we're going to
    ship in jessie.
    Thanks to Lucas Nussbaum <lucas@lucas-nussbaum.net> (Closes: #768697)
  * Build-Depend on ruby-test-unit to fix test runner failure

 -- Christian Hofstaedtler <zeha@debian.org>  Sun, 09 Nov 2014 16:34:41 +0100

jekyll (2.2.0+dfsg-1) unstable; urgency=medium

  * Imported Upstream version 2.2.0+dfsg
  * Refresh patches
  * Update Depends/Build-Depends
    + add ruby-jekyll-{paginate,gist,watch}

 -- Youhei SASAKI <uwabami@gfd-dennou.org>  Wed, 30 Jul 2014 18:21:02 +0900

jekyll (2.0.3+dfsg-1) unstable; urgency=low

  [Harlan Lieberman-Berg]
  * New upstream version.
  * Add new files to d/copyright.
  * New versions of jekyll don't use modernizr.
  * Install the upstream changelog.
  * Refresh patches for newer upstream version.

  [Youhei SASAKI]
  * Prepare d/changelog for upload
  * Disable some tests during build process
  * Update d/control: Refresh Depends and Build Depends
    + add ruby-rouge
  * Update d/rules:
    + add auto_build/auto_clean rules
    + add pre/post hook for test: create symlink for test
    + add original test files provided Gem
  * Add patches:
    + Remove newline for Maruku(=> 0.7.1), coffee-script
    + Fix relative LOAD_PATH in test_redcloth
    + Update test_rdiscount for newer RDiscount >= 2.0

 -- Youhei SASAKI <uwabami@gfd-dennou.org>  Tue, 03 Jun 2014 03:51:14 +0900

jekyll (1.5.1+dfsg1-2) unstable; urgency=medium

  [ Youhei SASAKI ]
  * Refresh patches, Drop obsolete patch (Closes: #746607)

  [ Dominique Dumont ]
  * control: added Depends on ruby-kramdows, ruby-rdiscount,
    ruby-redcloth (Closes: #712952)
  * forgot in last version: (Closes: #712954)
  * remove convenience copy of libjs-modernizr:
    * copyright file exludes libjs-moderniz.min.js
    + depends on libjs-modernizr
    + added a symlink using Debian's libjs-moderniz.min.js
    * watch file mangles version to add dfsg
  * updated copyright
  * added README.Debian

 -- Dominique Dumont <dod@debian.org>  Mon, 05 May 2014 18:01:01 +0200

jekyll (1.5.1-1) unstable; urgency=medium

  [ Cédric Boutillier ]
  * debian/control: remove obsolete DM-Upload-Allowed flag
  * use canonical URI in Vcs-* fields
  * debian/copyright: use DEP5 copyright-format/1.0 official URL for Format
    field

  [ Dominique Dumont ]
  * Imported Upstream version 1.5.1
  * control:
    * updated with cme, removed bogus dep on ruby-marku (is maruku)
    * actual dep is on version 3.2 of ruby-activesupport
  * updated patch headers for DEP-3
  * refreshed patch
  * removed dependency on launchy and use xdg-open instead
  * removed debian/jekyll.doc (README.textile is gone)
  * debian/ruby-tests.rake: disable failing (well, all) tests

 -- Dominique Dumont <dod@debian.org>  Tue, 29 Apr 2014 13:46:48 +0200

jekyll (0.11.2-1) unstable; urgency=low

  * Initial release (Closes: #672509)

 -- Youhei SASAKI <uwabami@gfd-dennou.org>  Tue, 08 May 2012 18:44:34 +0900
