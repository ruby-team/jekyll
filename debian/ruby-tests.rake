#-*- mode: ruby; coding: utf-8 -*-
require 'gem2deb/rake/testtask'

task :default => :test

exclude = %w[
  test/test_new_command.rb
  test/test_win_tz.rb
]

ENV["TZ"] = "UTC"

Gem2Deb::Rake::TestTask.new(:test) do |t|
  t.libs << 'lib' << 'test'
  if ENV['AUTOPKGTEST_TEST_NEW_COMMAND']
    t.test_files = FileList['test/test_new_command.rb']
  else
    t.test_files = FileList['test/test_*.rb'].exclude(/.*coffeescript.*|.*theme.*/) - exclude
  end
  t.verbose = true
end
